//
//  CollectionViewCell.swift
//  Papers
//
//  Created by Syncrhonous on 6/2/19.
//  Copyright © 2019 Syncrhonous. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var paperImageView: UIImageView!
    
    @IBOutlet weak var paperLabel: UILabel!
    
}
