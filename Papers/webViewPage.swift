//
//  webViewPage.swift
//  Papers
//
//  Created by Syncrhonous on 6/2/19.
//  Copyright © 2019 Syncrhonous. All rights reserved.
//

import UIKit
import WebKit
class webViewPage: UIViewController {

    @IBOutlet weak var webView: WKWebView!
    
    //var webAdd = "https://www.prothomalo.com"
    var webAdd: String? = UserDefaults.standard.string(forKey: "webAddress")


    override func viewDidLoad() {
        super.viewDidLoad()
        let myUrl = URL(string: webAdd!)
        let request=URLRequest(url: myUrl!)
        webView.load(request)

        // Do any additional setup after loading the view.
    }
    

}
