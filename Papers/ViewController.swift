//
//  ViewController.swift
//  Papers
//
//  Created by Syncrhonous on 6/2/19.
//  Copyright © 2019 Syncrhonous. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {

    @IBOutlet weak var collectionView: UICollectionView!
    let paperName = ["Amader Somoy","Ajkaler Khobor","Amader Orthonity","Gono Kontho","Prothom Alo","Vorer Dak","Bangladesh somoy","Kaler Kontho"]
    
    let paperUrl = [
        "https://dainikamadershomoy.com/","https://www.ajkalerkhobor.com/","https://amaderorthoneeti.com/new/","https://www.gonokantho.com/","https://www.prothomalo.com","https://bhorer-dak.com/","https://www.google.com/","https://www.kalerkantho.com/"
    ]
    
    
    let paperImage: [UIImage] = [
    UIImage(named: "amader_somoy")!,
    UIImage(named: "ajkaler_khobor")!,
    UIImage(named: "amader_orthonity")!,
    UIImage(named: "gono_khontho")!,
    UIImage(named: "prothom_alo")!,
    UIImage(named: "vorer_dak")!,
    UIImage(named: "bangladesh_somoy")!,
    UIImage(named: "kaler_kontho")!
    ]
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.delegate=self
        collectionView.dataSource=self
        
        var layout = self.collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.sectionInset = UIEdgeInsets.init(top: 0, left: 5, bottom: 0, right: 5)
        layout.minimumInteritemSpacing=5
        layout.itemSize = CGSize(width: (self.collectionView.frame.size.width-20)/2, height: self.collectionView.frame.size.height/3)
        // Do any additional setup after loading the view, typically from a nib.
    }

    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return paperName.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! CollectionViewCell
        
        cell.paperLabel.text = paperName[indexPath.row]
        cell.paperImageView.image = paperImage[indexPath.row]
        cell.layer.borderColor = UIColor.lightGray.cgColor
        cell.layer.borderWidth = 0.5
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath)
        cell?.layer.borderColor = UIColor.gray.cgColor
        cell?.layer.borderWidth = 2
        
        UserDefaults.standard.set(paperUrl[indexPath.row], forKey: "webAddress")
        
        UserDefaults.standard.synchronize()
        self.performSegue(withIdentifier: "connector", sender: self)
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath)
        cell?.layer.borderColor = UIColor.lightGray.cgColor
        cell?.layer.borderWidth = 0.5
    }

}

